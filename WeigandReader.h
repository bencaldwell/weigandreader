/*
WeigandReader.h - Library for reading from the cheap RFID reader with Weigand interface.

Pinouts:
Red: VCC +12V (5V from Arduino works)
Black: GND
Green: D0
Yellow: D1
White: LED (pull down to change from red to green)
Blue: BUZZER (pull down to buzz)

The reader automatically buzzes and sets LED to green when a tag has been read.

Borrowed heavily from http://www.hux.net.au/wp-content/uploads/2012/09/wiegand_door_access_module.ino

*/

#ifndef WeigandReader_h
#define WeigandReader_h

#include "Arduino.h"

class WeigandReader
{
 public:
  WeigandReader();
  int Read(byte** byteBuffer);
  void WeigandZero(void);
  void WeigandOne(void);

 private:  
  volatile unsigned long _tagId; //Need to use volatile for variables that can be changed outside of the current section of code e.g. interrupts
  volatile long _lastBitArrivalTime; //Time of last DO or D1 low
  volatile int _bitCount; //The number of bits read from the Weigand interface
  byte _cardBytes[4];
};

#endif
