# WeigandReader

WeigandReader is an Arduino library providing easy reading from the Weigand interface of an RFID reader.

#Acknowledgements

This library was derived from the examples provided at [www.hux.net.au][2]

#Usage

A simple [example sketch][1] is provided in the examples folder of the library repo.

[1]: https://bitbucket.org/bencaldwell/weigandreader/src/c228791a7cbd527aa4fda5fdf351749e26101aa1/examples/ExampleUsage/ExampleUsage.ino?at=master
[2]: http://www.hux.net.au/wp-content/uploads/2012/09/wiegand_door_access_module.ino