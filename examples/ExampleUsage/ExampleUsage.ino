#include <WeigandReader.h>

WeigandReader reader;

void interruptInput2()
{
  reader.WeigandZero();
}

void interruptInput3()
{
  reader.WeigandOne();
}

void setup()
{
  reader = WeigandReader();
  
  delay(1000); //Delay to avoid false interrupts

  //DO - Green wire
  attachInterrupt(0, interruptInput2, FALLING); //Call weigandZero when D0 signal falls. Interrupt 0 is on pin 2.
  
  //D1 - Yellow wire
  attachInterrupt(1, interruptInput3, FALLING); //Call weigandOne when D1 signal falls. Interrupt 1 is on pin 3.

  //start serial connection
  Serial.begin(9600);
  
}

void loop()
{
int numberOfBytesRead; //variable to store the number of bytes read from the card

  byte* cardBytes; //pointer to an array of bytes read from the card
  
  /*Get the bytes read from the card. 
  NOTE the second level of indirection (&cardBytes), a pointer to a pointer must be passed.
  The Read function must point the cardBytes pointer to a new array before returning
  so it needs access to the memory the pointer lives at, not just the pointer value.
  */
  numberOfBytesRead = reader.Read(&cardBytes);
  
  if (numberOfBytesRead) {
    Serial.print("Card Bytes: ");
    for (int i=0; i<numberOfBytesRead; i++) {
      Serial.print(cardBytes[i], HEX);
      Serial.print(" ");
    }
    Serial.println();
  }   
}
