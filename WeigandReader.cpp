#include "Arduino.h"
#include "WeigandReader.h"

// #define DEBUG

WeigandReader::WeigandReader()
{
  //configure D0 and D1 using internal pull-up resistor
  pinMode(2, INPUT_PULLUP); // Data0
  pinMode(3, INPUT_PULLUP); // Data1

  _bitCount = 0;
  _tagId = 0;
}

/**
Return the number of bytes read. The actual bytes are returned via the byte* byteBuffer.
 */
int WeigandReader::Read(byte** byteBuffer)
{
   if (_bitCount > 0 && millis() - _lastBitArrivalTime > 250) {

     int bytesRead = (_bitCount + (8-1)) / 8; //Round up to the nearest byte i.e. when 26 bits are read call it 4 bytes
     
      //Convert the 32 bits into 4 byte arrays
     _cardBytes[0] = lowByte(_tagId);
     _cardBytes[1] = highByte(_tagId);
     _cardBytes[2] = lowByte(_tagId >> 16);
     _cardBytes[3] = highByte(_tagId >> 16);

     #ifdef DEBUG
     Serial.print(_bitCount);
     Serial.print(" bits read, ");
     Serial.print(bytesRead);
     Serial.print(" bytes read, ");
     Serial.print(" tagID ");
     Serial.println(_tagId);
     #endif

     _tagId = 0;
     _bitCount = 0;

     #ifdef DEBUG
     Serial.print("byteBuffer before assignment: ");
     Serial.println((long)*byteBuffer, HEX);
     Serial.print("_cardBytes: ");
     Serial.println((long)_cardBytes, HEX);
     #endif

     *byteBuffer = _cardBytes;

     #ifdef DEBUG
     Serial.print("byteBuffer after assignment: ");
     Serial.println((long)*byteBuffer, HEX);
     #endif

     return bytesRead;
   }
   else {
     byteBuffer = 0;
     return 0;
   }
}

/**
If D0 is pulled down by the reader read a '0' bit.
 */
void WeigandReader::WeigandZero(void)
{
  //If it has been longer than 1s since the last bit then clear and start again
  if ( millis() - _lastBitArrivalTime > 1000 || _bitCount > 32) {
    _tagId = 32;
    _bitCount = 0;

    #ifdef DEBUG
    Serial.println();
    #endif
  }

  _lastBitArrivalTime = millis();
  _bitCount++;
  _tagId <<= 1; //Shift the tag ID bits left by one, the lowest bit remains at 0

  #ifdef DEBUG
  Serial.print("0");
  #endif
}

/**
If  D1 is pulled down by the reader read a '1' bit.
 */
void WeigandReader::WeigandOne(void)
{
  //If it has been longer than 1s since the last bit then clear and start again
  if ( millis() - _lastBitArrivalTime > 1000 || _bitCount > 32) {
    _tagId = 0;
    _bitCount = 0;

    #ifdef DEBUG
    Serial.println();
    #endif
  }

  _lastBitArrivalTime = millis();
  _bitCount++;
  _tagId <<= 1; //Shift the tag ID bits left by one
  _tagId |= 1; //Set the lowest bit

  #ifdef DEBUG
  Serial.print("1");
  #endif
}
